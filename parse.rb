require "nokogiri"
require "cgi"
require 'open-uri'
require 'net/http'
require 'pry'

class Word
  def self.logger(name, value)
    debug = false
    p "#{name} -> #{value}" if debug
  end

  def self.doParse
    pas = ""
    @PoemTextbox = <<-CODE
青鞋踏遍莓苔迹，万里桥南风瑟瑟。一双白鹭也多情，隔岸飞来如送客。
画桥南畔行人立，芦苇萧萧船去急。云连北阙与天深，月落西山和露白。
玉颜惆怅江波隔，夜夜流尘空宝瑟。可怜蓬鬓已惊风，好在葵心犹向日。
菱花半掩无消息，肠断城头秋月白。故园能隔几长亭？羌笛一声归未得。

  CODE
    puts "=============================="
    puts @PoemTextbox
    puts "=============================="
    @StartAnalyze = "开始分析"

    @doc = Nokogiri::HTML(open("http://sou-yun.com/AllusionInfer.aspx"), nil, 'utf-8')

    @viewstate = @doc.css('input#__VIEWSTATE').first
    logger "@viewstate", @viewstate[:value]

    @viewstategenerator = @doc.css('input#__VIEWSTATEGENERATOR').first
    logger "@viewstategenerator", @viewstategenerator[:value]

    @evetvalidation = @doc.css('input#__EVENTVALIDATION').first
    logger "@evetvalidation", @evetvalidation[:value]


    params = {'__VIEWSTATE' => @viewstate[:value], 
      '__VIEWSTATEGENERATOR' => @viewstategenerator[:value], 
      '__EVENTVALIDATION' => @evetvalidation[:value],
      'PoemTextbox' => @PoemTextbox,
      'StartAnalyze' => @StartAnalyze
    }

    url = URI.parse('http://sou-yun.com/AllusionInfer.aspx')
    logger "uri:params", params

    postData = Net::HTTP.post_form(url, params)
    logger "postData", postData.body


    result = Nokogiri::HTML(postData.body, nil, 'utf-8')
    # puts "=============================="
    # puts postData.body
    # puts "=============================="
    
    phrases = result.xpath('//div[@class="fullBlock"]/div[@class="fixComment"]')
    phrases.each_with_index do |phrase, pnum|
      #跳过所有空的p标签
      next if phrase.text.to_s.strip.length == 0
      explainStr = []
      header = phrase.css(".header").first
      #要解释词
      webConst = 1
      name = header.children[webConst].text.strip.delete("\u3000")
      explainStr << name
      explain = phrase.children[webConst].text.strip.delete("\u3000")
      index1 = explain.rindex("\u3002")
      index1 = explain.length if index1.nil?
      explain = explain.slice(0, index1+1) 
      explainStr << "\uFF1A#{explain}"

      handoff = false
      phrase.children.each do |tag|
        break if tag.nil?
        if handoff == true
          eTmp = tag.text.strip.delete("\u3000")
          index = eTmp.rindex("\u3002")
          index = eTmp.length if index.nil?
          eTmp = eTmp.slice(0, index+1)
          explainStr << eTmp
          handoff = !handoff
        end
        if tag.node_name == "br"
          handoff = true
        end
      end

      e = explainStr.join
      pas << e+"\n"
      # puts e
    end

  IO.popen('pbcopy', 'w') { |f| f << pas }
  puts pas
  end
end

Word.doParse
