# encoding: UTF-8

require "nokogiri"
require 'open-uri'
require 'pry'
require 'pry-nav'
require 'net/http'

def anaOri
	@doc = nil
	#@words 是所有可解释的词语
	@words = []
	File.open("./untitled.html", "w:UTF-8") do |f|
		@doc = Nokogiri::HTML(f, nil, "utf-8")
	end 
	phrases = @doc.xpath('//div[@class="fullBlock"]/div[@class="fixComment"]/preceding-sibling::p')
	# phrases 找到所有p标签
	phrases.each_with_index do |phrase, pnum|
		#跳过所有空的p标签
		next if phrase.text.to_s.strip.length == 0
		as = phrase.css("a")
		#解析所有链接 每一个a都是可以解释的词语
		as.each_with_index do |a_word, num|
			@words << a_word.text.strip
		end
	end

	puts @words.join(",")
end

#anaOri

@doc = nil
@words = []
File.open("./untitled.html") do |f|
	@doc = Nokogiri::HTML(f, nil, "utf-8")
end 
phrases = @doc.xpath('//div[@class="fullBlock"]/div[@class="fixComment"]')
phrases.each_with_index do |phrase, pnum|
	#跳过所有空的p标签
	next if phrase.text.to_s.strip.length == 0
	explainStr = []
	header = phrase.css(".header")
	#要解释词
    webConst = 1
    name = header.children[webConst].text.strip.delete("\u3000")
    explainStr << name
    explain = phrase.children[webConst].text.strip.delete("\u3000")
    index1 = explain.rindex("\u3002")
    index1 = explain.length if index1.nil?
    #binding.pry
    explain = explain.slice(0, index1+1) 
	explainStr << "\uFF1A#{explain}"

	handoff = false
	phrase.children.each do |tag|
		break if tag.nil?
		if handoff == true
			eTmp = tag.text.strip.delete("\u3000")
			index = eTmp.rindex("\u3002")
            # binding.pry
            index = -2 if index.nil?
			eTmp = eTmp.slice(0, index+1) 
			explainStr << eTmp
			handoff = !handoff
		end
		if tag.node_name == "br"
			handoff = true
		end
	end

	puts explainStr.join

end
